# Testing Fitur pada Produk SPLE PRICES-IDE target deployment GCP
**Berikut merupakan repository selenium testing pada produk PRICES-IDE yang dideploy pada target platform GCP**

## Tutorial
- Install package dengan run  `npm install`.
- Pengaturan input ataupun url terdapat pada file `env.js`.
- Run skenario test. Contoh: `node aboutus.js`.