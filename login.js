// Import the Selenium WebDriver
const { Builder, By, Key, until } = require('selenium-webdriver');
const { AMANAH_URL, EMAIL, PASSWORD } = require('./env');

// Create a new instance of the Chrome web browser
(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    // Navigate to homepage
    await driver.get(`${AMANAH_URL}`);
    await driver.sleep(5000);
    
    // Find the element with href="/login" using XPath
    let loginLink = await driver.findElement(By.xpath('//a[@href="/login"]'));
    await loginLink.click();
    await driver.sleep(5000);
 
    // Locate the email
    let emailInput = await driver.findElement(By.css('input[type="email"]'));
    await emailInput.sendKeys(EMAIL);
    // Locate the password
    let passwordInput = await driver.findElement(By.css('input[type="password"]'));
    await passwordInput.sendKeys(PASSWORD);
    // Find the form element using XPath
    let formElement = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/div/div/div/div/form'));
    let loginButton = await formElement.findElement(By.xpath('.//button'));
    await loginButton.click();
    await driver.sleep(5000);
    
    let logoutButton = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/nav/button'));
    // Find the text
    let childText = await logoutButton.getText();

    if (!childText.includes('Keluar')) {
      throw new AssertionError('Keluar not found');
    } else {
      console.log('Keluar button found');
    }
  } finally {
    await driver.sleep(3000);
    // Close the browser
    await driver.quit();
  }
})();
