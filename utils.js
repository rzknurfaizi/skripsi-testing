const chrome = require('selenium-webdriver/chrome');

const screenResolution = { width: 1920, height: 1080 };

// Configure Chrome options
const options = new chrome.Options();
options.addArguments(`--window-size=${screenResolution.width},${screenResolution.height}`);

module.exports = {
  options
}