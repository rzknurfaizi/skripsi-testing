const AMANAH_URL = 'https://procom.procom-rzk.cfd/';
// const AMANAH_URL = 'https://hightide.amanah.cs.ui.ac.id/';
// const AMANAH_URL = 'https://hightide.procom-rzk.cfd/';
const EMAIL = 'admin@user.com';
const PASSWORD = '123456';

const DONATUR = 'marine';
const DONATUR_EMAIL = 'admin@user.com';
const NO_TELEPON = '08188965743';
const JUMLAH_DONASI = 5000;
const TANGGAL_TRANSFER = "2024-03-06";
const METODE_PEMBAYARAN = 'cash';
const FILE_INPUT = 'D:\\Kuliah Fasilkom\\smt8\\ta\\doing\\code\\skripsi-testing\\Capture1.PNG';
const KETERANGAN = 'kawaii';

const ADD_ACTIVITY_NAMA_PROGRAM = 'test2';
const ADD_ACTIVITY_DESKRIPSI = 'test';
const ADD_ACTIVITY_TARGET = 'test';
const ADD_ACTIVITY_PARTNER = 'test';
const ADD_ACTIVITY_FILE_INPUT = 'D:\\Kuliah Fasilkom\\smt8\\ta\\doing\\code\\skripsi-testing\\Capture1.PNG';
const ADD_ACTIVITY_TANGGAL_PELAKSANAAN = "2024-03-06";

module.exports = {
  AMANAH_URL, EMAIL, PASSWORD,
  DONATUR,
  DONATUR_EMAIL,
  NO_TELEPON,
  JUMLAH_DONASI,
  TANGGAL_TRANSFER,
  METODE_PEMBAYARAN,
  FILE_INPUT,
  KETERANGAN,

  ADD_ACTIVITY_NAMA_PROGRAM,
  ADD_ACTIVITY_DESKRIPSI,
  ADD_ACTIVITY_TARGET,
  ADD_ACTIVITY_PARTNER,
  ADD_ACTIVITY_FILE_INPUT,
  ADD_ACTIVITY_TANGGAL_PELAKSANAAN,
};