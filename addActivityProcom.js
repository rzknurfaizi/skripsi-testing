// Import the Selenium WebDriver
const { Builder, By, Key, until } = require('selenium-webdriver');
const { 
  AMANAH_URL, EMAIL, PASSWORD,
  ADD_ACTIVITY_NAMA_PROGRAM,
  ADD_ACTIVITY_DESKRIPSI,
  ADD_ACTIVITY_TARGET,
  ADD_ACTIVITY_PARTNER,
  ADD_ACTIVITY_FILE_INPUT,
  ADD_ACTIVITY_TANGGAL_PELAKSANAAN,
} = require('./env');
const {
  options
} = require('./utils');

// Create a new instance of the Chrome web browser
(async function example() {
  let driver = await new Builder()
    .forBrowser('chrome')
    .setChromeOptions(options)
    .build();
  try {
    // Navigate to homepage
    await driver.get(`${AMANAH_URL}`);
    await driver.sleep(5000);
    
    // Find the element with href="/login" using XPath
    let loginLink = await driver.findElement(By.xpath('//a[@href="/login"]'));
    await loginLink.click();
    await driver.sleep(5000);
 
    // Locate the email
    let emailInput = await driver.findElement(By.css('input[type="email"]'));
    await emailInput.sendKeys(EMAIL);
    // Locate the password
    let passwordInput = await driver.findElement(By.css('input[type="password"]'));
    await passwordInput.sendKeys(PASSWORD);
    // Find the form element using XPath
    let formElement = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/div/div/div/div/form'));
    let loginButton = await formElement.findElement(By.xpath('.//button'));
    await loginButton.click();

    // Find confirmation link
    await driver.sleep(3000);
    let selectedDropdown = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/nav/ul/li[1]/button'));
    await selectedDropdown.click();
    let activityLink = await driver.findElement(By.xpath('//a[@href="/activity"]'));
    await activityLink.click();
    
    // Wait for the ul element to be present in the DOM
    await driver.sleep(5000);
    const ulXpath = '/html/body/div/div/div[1]/div/div/div/div[2]/div[1]/ul';
    let ulElement = await driver.wait(until.elementLocated(By.xpath(ulXpath)), 10000);
    // Find all li elements within the ul element
    let liElements = await ulElement.findElements(By.css('li'));
    let numberOfLi = liElements.length;
    console.log('list before: ', numberOfLi);

    let addActivityButton = await driver.findElement(By.xpath('//a[@href="/activity/tambah"]'));
    await addActivityButton.click();
    
    // Fill the form
    await driver.sleep(3000);
    let program = await driver.findElement(By.css('input[name="name"]'));
    await program.sendKeys(ADD_ACTIVITY_NAMA_PROGRAM);
    let deskripsi = await driver.findElement(By.css('textarea[name="description"]'));
    await deskripsi.sendKeys(ADD_ACTIVITY_DESKRIPSI);
    let target = await driver.findElement(By.css('input[name="target"]'));
    await target.sendKeys(ADD_ACTIVITY_TARGET);
    let partner = await driver.findElement(By.css('input[name="partner"]'));
    await partner.sendKeys(ADD_ACTIVITY_PARTNER);
    const fileInput = await driver.wait(until.elementLocated(By.xpath('//input[@type="file"]')));
    await fileInput.sendKeys(ADD_ACTIVITY_FILE_INPUT);
    let tanggalTransfer = await driver.findElement(By.css('input[name="executionDate"]'));
    await tanggalTransfer.sendKeys(ADD_ACTIVITY_TANGGAL_PELAKSANAAN);

    // Submit
    const submitButton = await driver.findElement(By.css('button[type="submit"]'));
    await submitButton.click();
    await driver.sleep(5000);

    await driver.sleep(5000);
    ulElement = await driver.wait(until.elementLocated(By.xpath(ulXpath)), 10000);
    // Find all li elements within the ul element
    liElements = await ulElement.findElements(By.css('li'));
    let numberOfLiAfterInsert = liElements.length;

    if (numberOfLi + 1 !== numberOfLiAfterInsert) {
      throw new AssertionError('Failed adding activity');
    } else {
      console.log('New Activity Added');
    }
  } finally {
    // Close the browser
    await driver.sleep(3000);
    await driver.quit();
  }
})();
