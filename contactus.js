// Import the Selenium WebDriver
const { Builder, By, Key, until } = require('selenium-webdriver');
const { AMANAH_URL } = require('./env');

// Create a new instance of the Chrome web browser
(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    // Navigate to homepage
    await driver.get(`${AMANAH_URL}`);
    await driver.sleep(5000);

    // Find dropdown
    let secondButton = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/nav/ul/li[4]/button'));
    await secondButton.click();
    
    // Find the anchor element with href="/contacts" using XPath
    let contactLink = await driver.findElement(By.xpath('//a[@href="/contacts"]'));
    await contactLink.click();
    await driver.sleep(5000);

    let title = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/div/body/section/div/div/div[1]'));
    // Find the text
    let childText = await title.getText();

    if (!childText.includes('Kontak Kami')) {
      throw new AssertionError('Kontak Kami not found');
    } else {
      console.log('Kontak Kami found');
    }
  } finally {
    await driver.sleep(3000);
    // Close the browser
    await driver.quit();
  }
})();
