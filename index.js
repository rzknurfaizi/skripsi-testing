// Import the Selenium WebDriver
const { Builder, By, Key, until } = require('selenium-webdriver');
const { 
  AMANAH_URL, EMAIL, PASSWORD,
  DONATUR,
  DONATUR_EMAIL,
  NO_TELEPON,
  JUMLAH_DONASI,
  TANGGAL_TRANSFER,
  METODE_PEMBAYARAN,
  FILE_INPUT,
  KETERANGAN 
} = require('./env');

// Create a new instance of the Chrome web browser
(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    // Navigate to homepage
    await driver.get(`${AMANAH_URL}`);
    await driver.sleep(5000);
    
    // Find the element with href="/login" using XPath
    let loginLink = await driver.findElement(By.xpath('//a[@href="/login"]'));
    await loginLink.click();
    await driver.sleep(5000);
 
    // Locate the email
    let emailInput = await driver.findElement(By.css('input[type="email"]'));
    await emailInput.sendKeys(EMAIL);
    // Locate the password
    let passwordInput = await driver.findElement(By.css('input[type="password"]'));
    await passwordInput.sendKeys(PASSWORD);
    // Find the form element using XPath
    let formElement = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/div/div/div/div/form'));
    let loginButton = await formElement.findElement(By.xpath('.//button'));
    await loginButton.click();

    // Find confirmation link
    await driver.sleep(3000);
    let selectedDropdown = await driver.findElement(By.xpath('/html/body/div/div/div[1]/div/nav/ul/li[3]/button'));
    await selectedDropdown.click();
    let confirmationLink = await driver.findElement(By.xpath('//a[@href="/confirmation"]'));
    await confirmationLink.click();
    
    // Fill the form
    await driver.sleep(3000);
    let donatur = await driver.findElement(By.css('input[name="name"]'));
    await donatur.sendKeys(DONATUR);
    let email = await driver.findElement(By.css('input[name="email"]'));
    await email.sendKeys(DONATUR_EMAIL);
    let telepon = await driver.findElement(By.css('input[name="phone"]'));
    await telepon.sendKeys(NO_TELEPON);
    let jumlahDonasi = await driver.findElement(By.css('input[name="amount"]'));
    await jumlahDonasi.sendKeys(JUMLAH_DONASI);
    let tanggalTransfer = await driver.findElement(By.css('input[name="date"]'));
    await tanggalTransfer.sendKeys(TANGGAL_TRANSFER);
    let metodePembayaran = await driver.findElement(By.css('input[name="paymentMethod"]'));
    await metodePembayaran.sendKeys(METODE_PEMBAYARAN);
    const fileInput = await driver.wait(until.elementLocated(By.xpath('//input[@type="file"]')));
    await fileInput.sendKeys(FILE_INPUT);
    let keterangan = await driver.findElement(By.css('input[name="description"]'));
    await keterangan.sendKeys(KETERANGAN);
    const option = await driver.findElement(By.css('select[name="idprogram"] option[value="11"]'));
    await option.click();

    // Submit
    const submitButton = await driver.findElement(By.css('button[type="submit"]'));
    await submitButton.click();
  } finally {
    // Close the browser
    await driver.sleep(3000);
    await driver.quit();
  }
})();
